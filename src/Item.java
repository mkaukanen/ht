
import java.util.ArrayList;
import java.util.Arrays;



/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */


public class Item {
    String name;
    public boolean fragile;
    protected double length;
    protected double height;
    protected double width;
    protected double mass;
    public String measures;
    
    public Item(String itemName, String itemSize, String itemMass, boolean fragility){
        name = itemName;
        String sizeparts[] = itemSize.split(";");
        length = Double.parseDouble(sizeparts[0]);
        width = Double.parseDouble(sizeparts[1]);
        height = Double.parseDouble(sizeparts[2]);
        mass = Double.parseDouble(itemMass);
        fragile = fragility;
        measures = (length+";"+width+";"+height).toString();
        DeliveryLog dl = DeliveryLog.getDeliveryLog();
        dl.addItemToHistory(name);
        dl.addNewItem(this);
    }
      
    //rakentaja joka ei ota parametrejä subclassi olioiden luomiseen
    public Item(){
    
    }
    
}

class Phone extends Item{
    
    public Phone(){
        name = "Älypuhelin";
        fragile = true;
        length = 30;
        width = 8;
        height = 12;
        measures = (length+";"+width+";"+height).toString();
    }
}   

class DogfoodParcel extends Item{
    
    public DogfoodParcel(){
        name = "Koiranruoka";
        fragile = false;
        length = 70;
        width = 30;
        height = 90;
        measures = (length+";"+width+";"+height).toString();
    }
}  

class BeerPack extends Item{
    
    public BeerPack(){
        name = "Karjala 6-pack";
        fragile = true;
        length = 42;
        width = 30;
        height = 12;
        measures = (length+";"+width+";"+height).toString();
    }
}  

class Fridge extends Item{
    
    public Fridge(){
        name = "Jääkaappi";
        fragile = true;
        length = 50;
        width = 60;
        height = 220;
        measures = (length+";"+width+";"+height).toString();
    }
}  
//##-Esineluokkia vähintään 4 kappaletta, joilla on erilaisia ominaisuuksia


