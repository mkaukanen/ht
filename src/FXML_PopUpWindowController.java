

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.stage.Stage;



/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class FXML_PopUpWindowController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<String> itemsBox;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private CheckBox breakableCheck;
    private ComboBox<String> classBox;
    @FXML
    private Button infoWindowButton;
    @FXML
    private ComboBox<String> startLocationBox;
    @FXML
    private ComboBox<String> startStorageChoiceBox;
    @FXML
    private ComboBox<String> destinationBox;
    @FXML
    private ComboBox<String> destinationStorageChoiceBox;
    @FXML
    private TextField nameField;
    @FXML
    private Button cancelButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private RadioButton class1Button;
    @FXML
    private RadioButton class2Button;
    @FXML
    private RadioButton class3Button;
    @FXML
    private ToggleGroup classButtonGroup;
    @FXML
    private Label errorLabel;
    @FXML
    private Button clearButton;
    @FXML
    private Label secondaryLabel;
    //returnattavat, jälkikäteen gettereillä helpoin implementoida
    Object rPkg=null;
    String rStart;
    String rDestination;
    int rPkgClass;

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts();   
        for (int i =0; i <list.size(); i++){ //täytetään kaupunkiluettelot
            String toAdd = list.get(i).getCityLocation().toUpperCase();
            if (!startLocationBox.getItems().contains(toAdd)){
                startLocationBox.getItems().add(toAdd);
            }
            if (!destinationBox.getItems().contains(toAdd)){
                destinationBox.getItems().add(toAdd);
            }
        }
        DeliveryLog dl = DeliveryLog.getDeliveryLog();
        List<String> itemlist = dl.getItemHistory(); //täytetään esinevalinta luettelo
        for (int i=0; i<itemlist.size(); i++){
            itemsBox.getItems().add(itemlist.get(i));
        }
        clearButton.setVisible(false); //piilotetaan kunnes esine valittu
      
    }

    
    @FXML
    private void populateStartStorageBox(ActionEvent event) {
        startStorageChoiceBox.getItems().clear();
        SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts(); 
        for (int i =0; i <list.size(); i++){
            String city = list.get(i).getCityLocation().toUpperCase();
            String storage = list.get(i).getPostOffice();
            //käydään läpi mitä lisätään:
            //-> sama kaupunki kuin kaupungin valintaboksissa,
            //mutta jos valittu jokin automaatti kohdepaikaksi, ei lisätä sitä
            if (!startStorageChoiceBox.getItems().contains(storage)
                    &&city.equals(startLocationBox.getSelectionModel().getSelectedItem())
                    &&!storage.equals(destinationStorageChoiceBox.getSelectionModel().getSelectedItem())){
                    startStorageChoiceBox.getItems().add(storage);
                }
        }
    }

    @FXML
    private void populateDestinationStorageBox(ActionEvent event) {
        destinationStorageChoiceBox.getItems().clear();
        SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts(); 
        for (int i =0; i <list.size(); i++){
            String city = list.get(i).getCityLocation().toUpperCase();
            String storage = list.get(i).getPostOffice();
            //käydään läpi mitä lisätään:
            //-> sama kaupunki kuin kaupungin valintaboksissa,
            //mutta jos valittu jokin automaatti lähtöpaikaksi, ei lisätä sitä
            if (!destinationStorageChoiceBox.getItems().contains(storage)
                    &&city.equals(destinationBox.getSelectionModel().getSelectedItem())
                    &&!storage.equals(startStorageChoiceBox.getSelectionModel().getSelectedItem())){
                    destinationStorageChoiceBox.getItems().add(storage);
                }
    }
    }

    @FXML
    private void selectionCheckStart(ActionEvent event) { 
        //tarkistetaan jos käyttäjä valitsee aluksi alku ja loppukaupungin, ja sen jälkeen
        //alkupisteen, että loppupisteistä kyseinen automaatti otetaan pois
        String selectedItem = startStorageChoiceBox.getSelectionModel().getSelectedItem();
        if(destinationStorageChoiceBox.getItems().contains(selectedItem)){
            destinationStorageChoiceBox.getItems().remove(selectedItem);
        }
    }

    @FXML
    private void selectionCheckDestination(ActionEvent event) {
        //tarkistetaan jos käyttäjä valitsee aluksi alku ja loppukaupungin, ja sen jälkeen
        //loppupisteen, että alkupisteistä kyseinen automaatti otetaan pois
        String selectedItem = destinationStorageChoiceBox.getSelectionModel().getSelectedItem();
        if(startStorageChoiceBox.getItems().contains(selectedItem)){
            startStorageChoiceBox.getItems().remove(selectedItem);
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sendPackageButtonAction(ActionEvent event) {
        boolean packageValid = false;
        String notValidText2="Esine ei mahdu 2 luokan pakettiin.";
        try{
            String start = startStorageChoiceBox.getSelectionModel().getSelectedItem();
            String destination = destinationStorageChoiceBox.getSelectionModel().getSelectedItem();
            RadioButton chosen = (RadioButton) classButtonGroup.getSelectedToggle();
            String sizes = sizeField.getText().replace("*", ";"); //parsimista varten pakettiluokassa
            Item item = fetchItem(); //luodaan uusi esine syötettyjen tietojen perusteella
            int pkgClass = 0;
            Object pkg = null; //paketti pakko alustaa, jotta näkyy redirectHome:lle myöhemmin
            if (itemsBox.getSelectionModel().getSelectedItem()!=null){
                sizes = item.measures;
            }
            if (chosen == class1Button){
                pkgClass =1;
                pkg = (Class1Package) new Class1Package(sizes, item);
                if (((Class1Package)pkg).returnValue()==true){   //castataan olio paketiks
                    packageValid = true;
                }
            }
            else if (chosen == class2Button){
                pkgClass =2;
                pkg = (Package) new Class2Package(sizes, item);
                if (((Class2Package)pkg).returnValue()==true){
                    packageValid = true;
                }
            }
            else if (chosen == class3Button){
                pkgClass =3;
                pkg = (Class3Package)new Class3Package(sizes, item);
                if (((Class3Package)pkg).returnValue()==true){
                    packageValid = true;
                }
            }

            TIMO_GUIController home = new TIMO_GUIController();
            if (pkgClass!=0 && packageValid == true && start!=null 
                    && destination != null && item!=null){
                rPkg=pkg;
                rStart=start;
                rDestination = destination;
                rPkgClass = pkgClass;
                Stage stage = (Stage) sendPackageButton.getScene().getWindow();
                stage.close();  
            }
            else{
                if (packageValid == false && pkgClass==2){
                    errorLabel.setText(notValidText2);
                }
                else if (nameField.getText()!="" && massField.getText() != ""
                        && sizeField.getText()!=""){
                    errorLabel.setText("Tarkista syötetyt tiedot!");
                }
                else{
                    errorLabel.setText("Syötä tarvittavat tiedot!");
                }
        }
        }catch (Exception e){
            errorLabel.setText("Syötä tarvittavat tiedot!");
    }
    }

    
    private Item fetchItem(){
        String pickeditem = null;
        Item item = null;
        String itemSize = sizeField.getText().replace("*", ";"); // .matches() varten muutetaan * pois
        if (itemsBox.getSelectionModel().getSelectedItem()!=null){
            pickeditem = itemsBox.getSelectionModel().getSelectedItem();
            //luodaan uusi esine valinnan mukaan:
            if (itemsBox.getSelectionModel().getSelectedItem().equals("Älypuhelin")){
                item = new Phone();
            }
            else if (itemsBox.getSelectionModel().getSelectedItem().equals("Koiranruoka")){
                item = new DogfoodParcel();
            }
            else if (itemsBox.getSelectionModel().getSelectedItem().equals("Karjala 6-pack")){
                item = new BeerPack();
            }
            else if (itemsBox.getSelectionModel().getSelectedItem().equals("Jääkaappi")){
                item = new Fridge();
            }
            else{
                DeliveryLog dl = DeliveryLog.getDeliveryLog();
                List<Item> list = dl.getUserCreatedItems();
                for (Item i : list){
                    if(i.name.equals(itemsBox.getSelectionModel().getSelectedItem())){
                        item = new Item (i.name, i.measures, Double.toString(i.mass), i.fragile);
                        break;
                    }   
                }
            }
        }
        else if(nameField.getText()!=null && massField.getText() != null //tarkistetaan kaikki kentät on täytetty
                &&massField.getText().matches("\\d+")                   // ja että paino ja ja mitat on annettu
                && itemSize.matches("\\d+\\;\\d+\\;\\d+")){  // oikeassa muodosssa
                boolean fragility=false;
                if (breakableCheck.isSelected()){
                    fragility = true;
                }
                item = new Item(nameField.getText(), itemSize,
                massField.getText(), fragility);
                }
        return item;
    }
    
    
    
    //getterit main windowille
    public Object getPopUpDatapkg(){
        return rPkg;
    }
    public String getPopUpDatastart(){
        return rStart;
    }
    public String getPopUpDatadestination(){
        return rDestination;
    }    
    public int getPopUpDatapkgClass(){
        return rPkgClass;
    }
    
    
    @FXML
    private void openInfoWindowAction(ActionEvent event) {
        ClassInfoStage window = new ClassInfoStage();
    }

    @FXML
    private void clearComboBox(ActionEvent event) { //nappulalla tyhjennys
        itemsBox.getSelectionModel().clearSelection();
        clearButton.setVisible(false);
    }
    //tyhjennetään esineboksi mikäli käyttäjä alkaa syöttää omaa esinettä
    //jokaiselle laatikolle oma metodi, sillä cräshää muuten mikäli alkaa 
    //syöttämään muussa kuin järjestyksessä tekstikenttiä(?).

    @FXML
    private void clearComboBox2(KeyEvent event) {
        itemsBox.getSelectionModel().clearSelection();
        clearButton.setVisible(false);
    }
    @FXML
    private void clearComboBox3(KeyEvent event) {
        itemsBox.getSelectionModel().clearSelection();
        clearButton.setVisible(false);
    }

    @FXML
    private void showClearButton(ActionEvent event) {
        clearButton.setVisible(true);
    }

}
