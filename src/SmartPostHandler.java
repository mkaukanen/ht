
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 

//Luokka, joka pitää kirjaa SmartPost-olioista
public class SmartPostHandler {
    private List<SmartPost> SmartPostList = new ArrayList<SmartPost>();
    private LinkedHashMap<String, SmartPostLocation> SmartPostLocations = new LinkedHashMap<String, SmartPostLocation>();
    //LinkedHashMap toimii kuten hashmap mutta pitää alkiot siinä järjestyksessä, kun ne on lisätty
    static private SmartPostHandler SHP = null;
    private int SmartPostCount = 0;

    //SINGLETON
    public static synchronized SmartPostHandler getSmartPostHandler(){
        if (SHP==null){
            SHP = new SmartPostHandler();
        }
        return SHP;
    }
    
    //funktiot SmartPost ja SmartPostLocation lisäämiselle sille dedikoidulle listalle
    public void addSmartPost(SmartPost SP){
        SmartPostList.add(SP);
    }
    
    public void addSmartPostLocation(String office, SmartPostLocation SPL){
        SmartPostLocations.put(office,SPL);  
    }
   
    //getterit e.m. listoille   
    public List<SmartPost> getSmartPosts(){
        return SmartPostList;
    }
    
    public LinkedHashMap<String, SmartPostLocation> getSmartPostLocations(){
        return SmartPostLocations;
    }
    
    public ArrayList<String> findCoordinates(String start, String destination){ //palauttaa arraylistin jota createPath tarvii
        ArrayList<String> coords = new ArrayList<String>();    
        for (Map.Entry<String, SmartPostLocation> entry : SmartPostLocations.entrySet()) {
            if (entry.getKey().equals(start)){
                coords.add(entry.getValue().getCoordinates().getElement1().toString());
                //hakee hashmapista valuen, joka on pari, josta haetaan 1 elementti
                coords.add(entry.getValue().getCoordinates().getElement2().toString());
            }
        }
        for (Map.Entry<String, SmartPostLocation> entry : SmartPostLocations.entrySet()) {
            if (entry.getKey().equals(destination)){
                coords.add(entry.getValue().getCoordinates().getElement1().toString()); 
                //hakee hashmapista valuen, joka on pari, josta haetaan 1 elementti
                coords.add(entry.getValue().getCoordinates().getElement2().toString());
            }
        }
        return coords;
    }
    
    
}
