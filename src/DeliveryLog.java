
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

//##TODO:
//Pitää kirjaa ikkunassa / välilehdessä tai muussa hyvin suunnitellussa UI-komponentissa

public class DeliveryLog {
    private Pair<String, String> packageDeliveryLocations; //pitää kirjaa lähtö- ja saapumispaikoista
    private LinkedHashMap<Double, Pair> deliveryData = new LinkedHashMap<Double, Pair>(); //matkan pituuksista
    //LinkedHashMap toimii kuten hashmap mutta pitää alkiot siinä järjestyksessä, kun ne on lisätty
    static private DeliveryLog DL = null;
    private double PackageCount;
    private ArrayList<String> itemHistory = new ArrayList<String>(
            Arrays.asList("Älypuhelin", "Koiranruoka", "Karjala 6-pack", "Jääkaappi"));
    private ArrayList<Item> userCreatedItems = new ArrayList<Item>();
    
    //SINGLETON
    public static synchronized DeliveryLog getDeliveryLog(){
        if (DL==null){
            DL= new DeliveryLog();
        }
        return DL;
    }
    
    public Pair createLocationsPair(String start, String destination){
        packageDeliveryLocations = Pair.createPair(start, destination);
        return packageDeliveryLocations;
    }
    
    public void addToDeliveryData(double distance, String start, String destination){
        createLocationsPair(start, destination);
        deliveryData.put(distance, packageDeliveryLocations);
        PackageCount++; 
    }
    
    public double getPackageCount(){
        return PackageCount;
    }
    
    public void addPackageToLog(String location1, String location2, double distance){
        PackageCount++;
        packageDeliveryLocations = Pair.createPair(location1, location2); //luodaan pari lähtö- ja saapumispaikoista
        //matkan pituuden saa suoraan javascript rajapinnalta
        deliveryData.put(distance, packageDeliveryLocations);  
    }
    
    //arraylist paketin luomisessa tarvittavaan esineiden valintaan
    public void addItemToHistory(String name){
        if (!itemHistory.contains(name)){
            itemHistory.add(name);
        }
    }
    
    public ArrayList<String> getItemHistory(){
        return itemHistory;
    }
    
    public void addNewItem(Item item){
        userCreatedItems.add(item);
    }
    
    public ArrayList<Item> getUserCreatedItems(){
        return userCreatedItems;
    }
    
    public LinkedHashMap<Double,Pair> getdeliveryData(){
        return deliveryData;
    }
}
