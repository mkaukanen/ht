
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class ClassInfoStage extends Stage{
    
    public ClassInfoStage(){
        Parent root;    
        try {
            root = FXMLLoader.load(getClass().getResource("ClassInfo_FXMLDocument.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("ClassInfo_GUI.css"); //css tiedostosta muotoilut käyttöön
            this.setResizable(false);
            this.setScene(scene);
            this.show();
        } catch (IOException ex) {
            Logger.getLogger(ClassInfoStage.class.getName()).log(Level.SEVERE, null, ex);
        }
 
    }

}
