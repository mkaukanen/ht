

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 


//Huom... ainakin itsellä Netbeanssissa projektia ajaessa näyttää edelleen 
//"HT_Kaukanen_Kettunen (jfxsa-run)", vaikka projektin nimet ja tiedot vaihdettu "HT_Kaukanen",
//sillä Kettunen (Kettunen Joni) ei koskaan sitten koodannutkaan riviäkään...
//Tästä ei siis tarvitse välittää.

public class Mainclass extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TIMO_FXMLDocument.fxml"));
        //Mainclass ei ns. pääluokka, vaan oman logiikan mukaisesti nimetty Mainclass ohjelman alkupiste
        //DeliveryLog ja Storage ovat luokat, jotka sisältävät koko datavaraston
        //TIMO_GUIController hoitaa pääikkunan operaatiot kuten piirtämiset käyttämällä avukseen
        //muita ikkunoita, luokkia ja datavarastoja
        Scene scene = new Scene(root);
        scene.getStylesheets().add("TIMO_GUI.css"); //css tiedostosta muotoilut käyttöön
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    @Override  //overridataan stop metodi jotta pystytään tekemään tiedostoon kirjoitukset
    public void stop(){    //ohjelma suljettaessa
        ReceiptHandler RH = ReceiptHandler.getReceiptHandler();
        RH.writeReceipt();
    }
    
}
