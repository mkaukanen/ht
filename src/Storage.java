
import java.util.ArrayList;
import java.util.LinkedHashMap;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */


//Tehtävänannon mukainen varastoluokka, johon on mahdollista tallentaa jo luotuja paketteja
public class Storage {
    private LinkedHashMap<String, ArrayList<Object>> allInventorys = new LinkedHashMap<String, ArrayList<Object>>();
    //tallentaa automaattikohtaisesti listan sinne toimitetuista paketeista
    //LinkedHashMap toimii kuten hashmap mutta pitää alkiot siinä järjestyksessä, kun ne on lisätty
    private String storageLocation;
    private ArrayList<Object> storageInventory =null;
    private static Storage st= null;
    
    //SINGLETON
    public static synchronized Storage getStorage(){
        if (st==null){
            st = new Storage();      
        }
        return st;
    }

    public void addPackageToStorage(Object pkg, String location){ //eri pakettiluokista johtuen tallennetaan yleisesti olioina              
        boolean exists = false;
        if (storageInventory == null){ //jos lista on tyhjä, alustetaan ja lisätään paketti
            storageInventory = new ArrayList<Object>(); //lisätään luodessa uusi varastolista
            storageInventory.add(pkg);
            allInventorys.put(location, storageInventory); //lisätään varastokirjanpitoon uusi varasto
        }
        else{
            for (String key : allInventorys.keySet()){
                if (key.equals(location)){ //etsitään onko varasto jo olemassa
                    allInventorys.get(location).add(pkg); //jos löytyi, otetaan kyseisen varaston pakettilista ja lisätään paketti listalle
                    exists = true;
                    break;
                }
            }
            if (exists == false){ //varastoa ei löytynyt, luodaan uusi varasto ja lisätään paketti
                storageInventory = new ArrayList<Object>();
                storageInventory.add(pkg);
                allInventorys.put(location, storageInventory); 
            }
        }
    }
    
    public LinkedHashMap<String, ArrayList<Object>> getStorageData(){
        return allInventorys;
    }
    
}
