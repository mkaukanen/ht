

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 

public class ClassInfo_FXMLController implements Initializable {

    @FXML
    private TextArea packageInfoField;
    @FXML
    private Button OKButton;
    @FXML
    private ImageView package2Image;
    @FXML
    private ImageView package3Image;
    @FXML
    private ImageView package1Image;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image image1 = new Image("class1package.jpg"); //pakettikuvat kehiin
        package1Image.setImage(image1);
        Image image2 = new Image("class2package.jpg");
        package2Image.setImage(image2);
        Image image3 = new Image("class3package.jpg");
        package3Image.setImage(image3);
        String InfoFile = "PackageClassDescriptions.txt";
        BufferedReader in;
        try { //luetaan tiedot tiedostosta, vaatii jonkin verran lisää kikkailua 
            //kun tiedosto pitää olla src kansiossa
            InputStream input = getClass().getClassLoader().getResourceAsStream(InfoFile);
            in = new BufferedReader (new InputStreamReader(input, StandardCharsets.ISO_8859_1));
            String line;
            while ((line = in.readLine()) != null){   
                packageInfoField.appendText(line+"\n\n");
        } 
        in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClassInfo_FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClassInfo_FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }    

    @FXML
    private void exitWindowAction(ActionEvent event) {
        Stage stage = (Stage) OKButton.getScene().getWindow();
        stage.close();
    }
    
}
