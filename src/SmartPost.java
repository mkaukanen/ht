

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 


//SmartPost-luokka, joka sisältää automaattikohtaisen datan sekä osaluokka, 
//johon on tallennettu automaatin geograafinen paikka
public class SmartPost {
    private String openhours;
    private String city;
    private String postalCode;
    private String postalAddress;
    private String fullAddress;
    private String postOffice;
    
    public SmartPost(String code, String cityName, String address, String postoffice, String hours, String Xcoord, String Ycoord){
        openhours=hours;  //asetetaan tiedot SmartPostin jäsenmuuttujiin
        city = cityName;
        postalCode = code;
        postalAddress = address;
        fullAddress= postalAddress+", "+postalCode+" "+city;
        postOffice = postoffice;
        SmartPostHandler SPH = SmartPostHandler.getSmartPostHandler();
        SPH.addSmartPost(this); // lisää SmartPostin niistä listaa pitävään luokkaan 
    }
    //getterit:
    
    public String getOpenhours(){ //getteri, jolla saadaan yksittäisen SmartPostin aukioloajat
        return this.openhours;
    }
  
    public String getFullAddress(){
        return this.fullAddress;
    }
    
    public String getPostOffice(){
        return this.postOffice;
    }
    
    public String getCityLocation(){
        return this.city;  
    }
    
    public String getPostalAddress(){
        return this.postalAddress;
    }
    public String getPostalCode(){
        return this.postalCode;
    }
    
}
    class SmartPostLocation extends SmartPost{
        Pair<Double, Double> locationCoordinates;

        public SmartPostLocation(String code, String cityName, String address, String postoffice, String hours, String Xcoord, String Ycoord){
            super(code, cityName, address, postoffice, hours, Xcoord, Ycoord);
            //subclass rakentaja vaatii kaikki samat argumentit jotka super classin rakentaja
            double X = Double.parseDouble(Xcoord);
            double Y = Double.parseDouble(Ycoord);  
            locationCoordinates = Pair.createPair(X, Y);
            SmartPostHandler SPH = SmartPostHandler.getSmartPostHandler();
            
            SPH.addSmartPostLocation(postoffice, this); // lisää SmartPostin lokaation niistä listaa pitävään luokkaan 
        }
        
        public Pair getCoordinates(){
            return this.locationCoordinates;
        }
    }
