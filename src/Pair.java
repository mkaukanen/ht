/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 

//luokka jossa määritellään Pair -rakenne
public class Pair<first, second> {
    private final first element1;
    private final second element2;
    
    public static <first, second> Pair<first, second> createPair(first element1, second element2){
        return new Pair<first, second>(element1, element2);   //Pair.createPair kommenolla luodaan ja
                                                              //palautetaan uusi pari kutsuvaan luokkaan
    }
    
    public Pair(first element1, second element2){  //arvojen lisäys pariin
        this.element1 = element1;
        this.element2 = element2;
    }
    
    public first getElement1(){  //getterit parin elementeille
        return element1;
    }
    public second getElement2(){
        return element2;
    }
}
