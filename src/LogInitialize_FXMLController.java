
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class LogInitialize_FXMLController implements Initializable {
    private boolean choice;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;
 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        choice = false; //siltä varalta että käyttäjä sulkee ikkunan painamatta nappia
    }  
    
    public boolean getUserDecision(){
        return choice;
    }

    @FXML
    private void setChoiceTrue(ActionEvent event) {
        choice = true;  //choiceksi tosi ja suljetaan ikkuna
        Stage stage = (Stage) yesButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void setChoiceFalse(ActionEvent event) {
        choice = false;  //choiceksi epätosi ja suljetaan ikkuna
        Stage stage = (Stage) noButton.getScene().getWindow();
        stage.close();

    }
    
}
