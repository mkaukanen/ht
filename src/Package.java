
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 


//Pakettiluokka, josta on periytetty useampia pakettiluokkia
class Package {
    LinkedHashMap<String, Double> measures;
    //LinkedHashMap toimii kuten hashmap mutta pitää alkiot siinä järjestyksessä, kun ne on lisätty
    static String[] keys= new String[]{"lenght", "height", "width"};
    Double values[];
    Item itemInside; //pitää kirjaa paketeissa olevista esineistä
    boolean success;  //onnistuuko pakkaaminen / lähettäminen kyseisessä paketissa
          
    
    public LinkedHashMap<String, Double> createMap(String[] keys, Double[] values){
        measures = new LinkedHashMap<String, Double>();
        for (int i=0; i<keys.length; i++){
            measures.put(keys[i], values[i]);
        }
        return measures;
    }
    
}

class Class1Package extends Package{
    //Luokka 1. luokan paketeille, periytetty abstraktista Package luokasta.
    
    public Class1Package(String sizes, Item item){ //rakentaja
        String sizeparts[] = sizes.split(";");
        double L = Double.parseDouble(sizeparts[0]);
        double W = Double.parseDouble(sizeparts[1]);
        double H = Double.parseDouble(sizeparts[2]);
        values = new Double[]{L, H, W};
        measures = createMap(keys, values); //tallennetaan paketin mitat HashMappiin
        itemInside = item;
        success= true;
    }
    public boolean returnValue(){
        return success;
    }
}

class Class2Package extends Package{
    List<Double> Sizelimits; //luokka 2 kokorajoitteet
    //Luokka 1. luokan paketeille, periytetty abstraktista Package luokasta.
    
    public Class2Package(String sizes, Item item){ //rakentaja
        Sizelimits =  new ArrayList<Double>(Arrays.asList(100d,100d,100d));
        String sizeparts[] = sizes.split(";");
        double L = Double.parseDouble(sizeparts[0]);
        double W = Double.parseDouble(sizeparts[1]);
        double H = Double.parseDouble(sizeparts[2]);
        values = new Double[]{L,W,H};
        measures = createMap(keys, values); //tallennetaan paketin mitat HashMappiin
        for (int i=0; i<Sizelimits.size(); i++){
            if(Sizelimits.get(i)<values[i]){ 
                success=false;
            }
            else{
                success = true;
            }
        }
        itemInside = item;
    }
    public boolean returnValue(){
        return success;
    }
    
}

class Class3Package extends Package{
    //Luokka 1. luokan paketeille, periytetty abstraktista Package luokasta.
    public Class3Package(String sizes, Item item){ //rakentaja
        String sizeparts[] = sizes.split(";");
        double L = Double.parseDouble(sizeparts[0]);
        double W = Double.parseDouble(sizeparts[1]);
        double H = Double.parseDouble(sizeparts[2]);
        values = new Double[]{L, H, W};
        measures = createMap(keys, values); //tallennetaan paketin mitat HashMappiin
        itemInside = item;
        success = true;
    
    }    
    public boolean returnValue(){
        return success;
    }
}

