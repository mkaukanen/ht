
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 

//Luokka, joka lukee XML-muotoista dataa ja luo niistä SmartPost-olioita
public class XMLParser {
    Document doc;
    URL url;
    String locationName;
    
    //luetaan XML tiedosta tarpeelliset asiat, parsitaan ja lisätään tietokantaan
    public void fillDatabase() throws MalformedURLException, IOException{
       String source = "http://smartpost.ee/fi_apt.xml";
       url = new URL(source);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       InputStream input = url.openStream();
       try {
           DocumentBuilderFactory dbc = DocumentBuilderFactory.newInstance();
           DocumentBuilder db = dbc.newDocumentBuilder();
           doc = db.parse(input);
           doc.getDocumentElement().normalize();
           parseContent();
       } catch (ParserConfigurationException | SAXException | IOException ex) {
           Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
       }
       
   }
   
    public void parseContent() throws IOException{
       NodeList nodes = doc.getElementsByTagName("place");
       for (int i=0; i< nodes.getLength();i++){
        Node node = nodes.item(i);
        Element element = (Element) node;  //haetaan elementistä tunnistetiedoilla tarvittavat asiat
        String code = (String) element.getElementsByTagName("code").item(0).getTextContent();
        String cityName = (String) element.getElementsByTagName("city").item(0).getTextContent();
        String address = (String) element.getElementsByTagName("address").item(0).getTextContent();
        String postoffice = (String) element.getElementsByTagName("postoffice").item(0).getTextContent();
        String hours = (String) element.getElementsByTagName("availability").item(0).getTextContent();
        String Xcoord = (String) element.getElementsByTagName("lat").item(0).getTextContent();
        String Ycoord = (String) element.getElementsByTagName("lng").item(0).getTextContent();
        createSmartPost(code, cityName, address, postoffice, hours, Xcoord, Ycoord);
       }
    }
    
    public void createSmartPost(String code, String cityName, String address, String postoffice, String hours, String Xcoord, String Ycoord){ 
    //luodaan kaikista SmartPost lokaatiosta oliot, ja lisätään ne SmartPost luokasta SmartPostHandlerille,
    //jonka avulla kutsutaan käyttäjän valitseamia lokaatioita kartalle TIMO_GUIControllerissa
        SmartPost SP = new SmartPost(code, cityName, address, postoffice, hours, Xcoord, Ycoord);
        SmartPostLocation SPL = new SmartPostLocation(code, cityName, address, postoffice, hours, Xcoord, Ycoord);
    }


}
       