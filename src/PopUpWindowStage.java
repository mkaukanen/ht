
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class PopUpWindowStage extends Stage {
        
    public PopUpWindowStage(Parent root){
            Scene scene = new Scene(root); 
            scene.getStylesheets().add("PopUp_GUI.css"); //css tiedostosta muotoilut käyttöön
            this.setResizable(false);
            this.setScene(scene);
            this.showAndWait();
    }
}
