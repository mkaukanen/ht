
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */ 

public class TIMO_GUIController implements Initializable {
    ArrayList<String> pathCoordinates;
    @FXML
    private WebView mapBase;
    @FXML
    private Tab packageLogTab;
    @FXML
    private Tab smartPostTab;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TabPane tabPane;
    @FXML
    private StackPane bottomStackPane;
    @FXML
    private GridPane GridPane;
    @FXML
    private Button openWindowButton;
    @FXML
    private Button addCityButton;
    @FXML
    private ComboBox<String> citySelectionBox;
    @FXML
    private Pane imagePane;
    @FXML
    private Label textLabel;
    @FXML
    private Label infoLabelLeft;
    @FXML
    private Label InfoLabelRight;
    @FXML
    private Label infoLabelMiddle;
    @FXML
    private Rectangle rectangle;
    @FXML
    private Button deletePathsButton;
    @FXML
    private ListView<String> packageLogView;
    SingleSelectionModel<Tab> selectionModel;
    @FXML
    private Label packageCountLabel;
    @FXML
    private Label classCountLabel;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //kuvan lisäys "hankalammalla tavalla" ja jälkikäteen helposti muokattavasti
        //demonstroitu tässä
        ImageView image = new ImageView (new Image("timo-logo.jpg"));
        image.fitWidthProperty().bind(imagePane.widthProperty());
        image.fitHeightProperty().bind(imagePane.heightProperty());
        imagePane.getChildren().add(image);
        //kuvan lisäys tehty
        pathCoordinates = new ArrayList<String>();
        mapBase.getEngine().load(getClass().getResource("index.html").toExternalForm());
        //ladataan kartta ikkunaan näkyviin html tiedostosta.
        tabPane.tabMinWidthProperty().bind(anchorPane.widthProperty().divide(tabPane.getTabs().size()));
        //skaalaa tabien otsikot koko ikkunan leveydelle.
        InitializeCitySelectionBox(); 
        boolean choice;
        choice = InitializePackageLog();
        if (choice ==true){
            initializeLogPage();
        }
        packageCountLabel.setText("Yhteensä "+
                Integer.toString(packageLogView.getItems().size())+" lähetettyä pakettia.");
        updateClassCountLabel();
        if (packageLogView.getItems().isEmpty()){
            packageLogView.getItems().add("Pakettiloki on tyhjä.");
        }
  
    }
    
    //############################  SMARTPOST TAB  ################################//
    //############################  JA ALAPALKKI   ################################//
    /////////////////////////////////////////////////////////////////////////////////
    
    private void InitializeCitySelectionBox() {
        XMLParser xml = new XMLParser();
        try {
            xml.fillDatabase();
        } catch (IOException ex) {
            Logger.getLogger(TIMO_GUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
        SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts();
        for (int i =0; i <list.size(); i++){
            String toAdd = list.get(i).getCityLocation().toUpperCase();
            if (!citySelectionBox.getItems().contains(toAdd)){
                citySelectionBox.getItems().add(toAdd);
            }
        }

    }
    
    private boolean InitializePackageLog(){
        boolean choice =false; //vaatii alustuksen, jotta return ei herjaa
        Parent root;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("LogInitialize_FXMLDocument.fxml"));
        try {
            root = loader.load();
            LogInitializeWindowStage window = new LogInitializeWindowStage(root);
            LogInitialize_FXMLController controller = loader.getController();
            choice = controller.getUserDecision();
        } catch (IOException ex) {
            Logger.getLogger(TIMO_GUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return choice;
    }

    @FXML
    private void openNewWindowAction(ActionEvent event) {
        goToSmartPostTab();
        Parent root;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML_PopUpWindowDocument.fxml"));
        try {
            root = loader.load();
            PopUpWindowStage window = new PopUpWindowStage(root);
        
            FXML_PopUpWindowController controller = loader.getController();
            Object pkg = controller.getPopUpDatapkg();
            String start = controller.getPopUpDatastart();
            String destination = controller.getPopUpDatadestination();
            int pkgClass = controller.getPopUpDatapkgClass();
            deliverPackage(pkg, start, destination, pkgClass);
        } catch (IOException ex) {
            Logger.getLogger(TIMO_GUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void addCityAction(MouseEvent event) {
        goToSmartPostTab();
        String location = citySelectionBox.getSelectionModel().getSelectedItem();
        SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts();
        for (int i =0; i <list.size(); i++){
            String toAdd = list.get(i).getCityLocation().toUpperCase();
            if (toAdd.equals(location)){
                //haetaan tarvittavat asiat SmartPost luokasta javascrip rajapintaa varten
                String city = list.get(i).getCityLocation();
                String fullAddress = list.get(i).getFullAddress();
                String postoffice = list.get(i).getPostOffice();
                String hours = list.get(i).getOpenhours();
                String officeAndHours= postoffice+" "+hours;
                String color = "blue";
                mapBase.getEngine().executeScript( //javascript rajapinta
                        "document.goToLocation('"+fullAddress+"', '"+officeAndHours+"', '"+color+"')");
                textLabel.setText("Kaupungin "+city+" SmartPost automaatit lisätty kartalle.");
            }
            
            }
    }
    
    
    private void deliverPackage(Object pkg, String start, String destination, int pkgClass){
            if (pkg!=null){
            //paketti tuodaan objectina, koska muuten tuottaa ongelmia se, minkä luokan paketti ollaan tuomassa        
            boolean fragility = false; //default false
            if (pkg instanceof Class1Package){ //typecastataan paketti oikeaan luokkaan
                fragility = ((Class1Package) pkg).itemInside.fragile;
            }
            if (pkg instanceof Class2Package){ //typecastataan paketti oikeaan luokkaan
                fragility = ((Class2Package) pkg).itemInside.fragile;
            }
            if (pkg instanceof Class3Package){ //typecastataan paketti oikeaan luokkaan
                fragility = ((Class3Package) pkg).itemInside.fragile;
            }
            String condition=""; //tarkistetaan ehjyys vasta "kuljetuksen jälkeen", sillä olisi 
            //epäloogista määrittää tila rikkinäiseksi ennen kuin se varsinaisesti menisi rikki
            SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
            ArrayList<String> coords = sph.findCoordinates(start, destination);
            String color = "red"; 
            addStartAndEndLocations(start, destination); //lisää reitin alku ja loppupisteet kartalle
              
            //index.html muokattu itse siten, että 1.luokan paketteja joiden matka olisi 150km ei kuljeteta
            double distance = (Double) (mapBase.getEngine().executeScript( //javascript rajapinta
                        "document.createPath("+coords+", '"+color+"', "+pkgClass+")"));
            //javascript palauttaa jollain tietyillä smartpost lokaatioilla integerin,
            //vaikka oletusarvoisesti javascript ilmeisesti käsittelee doublena?
            //->ClassCastException
            //(ESIM: ÄHTÄRI Pakettiautomaatti S-market Ähtäri, AKAA Pakettiautomaatti Tokmanni Kylmäkoski)
            if(pkgClass==1 && distance >=150){
                condition ="EI KULJETETTU. 1. Luokan paketin matkarajoite: 150km. Matka: "+distance+"km";
                textLabel.setText("TIMO-mies ei kuljettanut pakettia, sillä 1.LK paketin matkarajoite on 150km (Matka: "+distance+"km)");
            }
            else if (pkgClass!=2 && fragility==true){
                condition = "Rikkoutunut kuljetuksessa";
                textLabel.setText("Esine kuljetettu, mutta meni rikki kuljetuksessa.");
            }
            else{
                condition = "Ehjä";
                textLabel.setText("Esine kuljetettu perille");
            }
            if(!(pkgClass==1 && distance>150.0)){ //1. lk pakettia jonka matka olisi ollut liian pitkä ei viedä varastotietoihin..
                DeliveryLog dl = DeliveryLog.getDeliveryLog();
                dl.addToDeliveryData(distance, start, destination);
                Storage st = Storage.getStorage();
                st.addPackageToStorage(pkg, destination);
            }
            else{
                distance = 0;
            }       
            //..mutta kirjataan lokiin epäonnistuneena lähetyksenä
            addToLogPage(pkg, start, destination, pkgClass, condition, distance);
            
            }
    }

    
    public void addStartAndEndLocations(String start, String destination){
         SmartPostHandler sph = SmartPostHandler.getSmartPostHandler();
        List<SmartPost> list = sph.getSmartPosts();  //lisää alku ja loppupisteen kartalle
            for (int i =0; i <list.size(); i++){
                if (list.get(i).getPostOffice().equals(start)){
                    String fullAddress = list.get(i).getFullAddress();
                    String officeAndHours = start +" "+ list.get(i).getOpenhours();
                    String dotColor = "blue";
                    mapBase.getEngine().executeScript( //javascript rajapinta
                        "document.goToLocation('"+fullAddress+"', '"+officeAndHours+"', '"+dotColor+"')");
                }
            }
            for (int j =0; j<list.size(); j++){
                if (list.get(j).getPostOffice().equals(destination)){
                    String fullAddress = list.get(j).getFullAddress();
                    String officeAndHours = destination +" "+ list.get(j).getOpenhours();
                    String dotColor = "blue";
                    mapBase.getEngine().executeScript( //javascript rajapinta
                        "document.goToLocation('"+fullAddress+"', '"+officeAndHours+"', '"+dotColor+"')");
                }
            }
    }
    
    @FXML
    private void deletePaths(MouseEvent event) {
        goToSmartPostTab();
        mapBase.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void goToSmartPostTab(ActionEvent event) { //jos tehdään alapalkissa toimintoja,
        selectionModel = tabPane.getSelectionModel();  //jotka liittyvät karttaan, siirrytään
        selectionModel.select(smartPostTab);           //sille tabille
    } 

    private void goToSmartPostTab() {                  //  -||-
        selectionModel = tabPane.getSelectionModel();
        selectionModel.select(smartPostTab);
    }
    
    //############################  PAKETTILOKI TAB  ################################//
    ///////////////////////////////////////////////////////////////////////////////////
  
    private void addToLogPage(Object pkg, String start, String destination,
            int pkgClass, String condition, double distance){
        //alunperin suunnitelmana oli hakea DeliveryLogista tiedot, mutta ne saa kätevämmin 
        //parametrien avulla kun samat asiat löytyvät jo deliverPackagesta.
        String dataMember;
        String receiptItem;
        DateFormat DF = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date date = new Date();
        String itemWas="";
        String measures="";
        if (pkg instanceof Class1Package){ //typecastataan paketti oikeaan luokkaan
                itemWas = ((Class1Package) pkg).itemInside.name; //esine
                measures = ((Class1Package) pkg).itemInside.measures;
            }
            if (pkg instanceof Class2Package){ //typecastataan paketti oikeaan luokkaan
                itemWas = ((Class2Package) pkg).itemInside.name; //esine
                measures = ((Class2Package) pkg).itemInside.measures;
            }
            if (pkg instanceof Class3Package){ //typecastataan paketti oikeaan luokkaan
                itemWas = ((Class3Package) pkg).itemInside.name;  //esine
                measures = ((Class3Package) pkg).itemInside.measures;
            }
        measures = measures.replace(";", "*"); //muotoillaan uusiksi
        measures = measures.replace(".0", ""); //trailing zerot pois
        dataMember = DF.format(date)+". Pakettiluokka: "+pkgClass+"  | Paketin koko: "
                +measures+"cm\nLähetetty esine: " +itemWas+"  | Tila: "+condition
                +"\nLähetyspiste: "+start+"  | Määränpää: "+destination
                +"  | "+distance+"km.";
        receiptItem = DF.format(date)+" "+start+".\r\n"
                +"SmartPost lähetys. Pakettiluokka "+pkgClass+".\r\n"
                +"Lähetyskohde: "+destination+".\r\n"
                +condition+".\n";
        ReceiptHandler RH = ReceiptHandler.getReceiptHandler();
        RH.addToReceipt(receiptItem);
        if (packageLogView.getItems().contains("Pakettiloki on tyhjä.")){
            packageLogView.getItems().clear();
        }
        packageLogView.getItems().add(dataMember);
        packageCountLabel.setText("Yhteensä "+
                Integer.toString(packageLogView.getItems().size())+" lähetettyä pakettia.");
        updateClassCountLabel();
        ExternalPackageLogHandler EPL = ExternalPackageLogHandler.getExternalPackageLogHandler();
        EPL.updateExternalPackageLog(dataMember);
    }
    
    private void initializeLogPage(){
        ExternalPackageLogHandler EPLogger = ExternalPackageLogHandler.getExternalPackageLogHandler();
        ArrayList<String> LogList = EPLogger.getExternalLogData();
        for (String s : LogList){
            if (s.length()>5){   
                s=s.replace(";", "\n");
                packageLogView.getItems().add(s);
           }   
        }
        
    }
    
    private void updateClassCountLabel(){
        String classNumber;
        int class1Count, class2Count, class3Count;
        class1Count = class2Count = class3Count =0;
        String pkgContent;
        if (packageLogView.getItems().size()>0){
            for (int i=0; i<packageLogView.getItems().size(); i++){
                pkgContent = packageLogView.getItems().get(i);
                classNumber = pkgContent.substring(33,34);
                if (classNumber.equals("1")){
                    class1Count+=1;
                }
                else if (classNumber.equals("2")){
                    class2Count+=1;
                }
                else if (classNumber.equals("3")){
                    class3Count+=1;
                }
            }
        }
        classCountLabel.setText("Luokka1:  "+class1Count+" pakettia.     "
                + "Luokka2:  "+class2Count+" pakettia.      "
                + "Luokka3:  "+class3Count+" pakettia.");
    }
     
}
