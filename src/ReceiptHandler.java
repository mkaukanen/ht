
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class ReceiptHandler {
    private ArrayList<String> receiptItems = new ArrayList<String>();
    private static ReceiptHandler RH = null;   
    
        //SINGLETON
    public static synchronized ReceiptHandler getReceiptHandler(){
        if (RH==null){
            RH= new ReceiptHandler();
        }
        return RH;
    }
    
    public void addToReceipt(String s){
        receiptItems.add(s);
    }
    
    public void writeReceipt(){ //kirjoitaa kuitin, kutustaan Mainclassista
                                //ohjelman sulkeutuessa
        DateFormat DF = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();  //jos haluttaisiin pitää kuittihistoriaa, voitaisiin lisätä
        //timestamppiin kellonaika. En kuitenkaan halua täyttää kansiotani kuiteilla. 
        String receiptName ="Receipt_"+DF.format(date)+".txt";
        BufferedWriter out;
        try {
            out = new BufferedWriter (new FileWriter(receiptName));
            for (String s : receiptItems){
                 out.write(s +"\r\n");
            } 
            out.write("SmartPost lähetyksiä yhteensä " +receiptItems.size()+" kappaletta.\r\n\r\n");
            out.write("Session jälkeinen varastotilanne:\r\n");
            Storage st = Storage.getStorage();
            LinkedHashMap<String, ArrayList<Object>> map = st.getStorageData();
            for (Map.Entry<String, ArrayList<Object>> entry : map.entrySet()){
                out.write(entry.getKey()+": "+entry.getValue().size()+" kappaletta paketteja.\r\n");
                out.write("Paketeissa sisällä:\r\n");
                for (int i=0; i<entry.getValue().size(); i++){
                    Object pkg = entry.getValue().get(i);
                    String details="";
                    if (pkg instanceof Class1Package){ //typecastataan paketti oikeaan luokkaan
                        details = ((Class1Package) pkg).itemInside.name+"  | Mitat: "+((Class1Package) pkg).itemInside.measures.replace(";", "*").replace(".0","")+"cm.";
                    }
                    if (pkg instanceof Class2Package){ //typecastataan paketti oikeaan luokkaan
                        details = ((Class2Package) pkg).itemInside.name+"  | Mitat: "+((Class2Package) pkg).itemInside.measures.replace(";", "*").replace(".0","")+"cm.";
                    }
                    if (pkg instanceof Class3Package){ //typecastataan paketti oikeaan luokkaan
                        details = ((Class3Package) pkg).itemInside.name+"  | Mitat: "+((Class3Package) pkg).itemInside.measures.replace(";", "*").replace(".0","")+"cm.";
                    }
                    out.write(details+"\r\n");
                    }
                out.write("\r\n");
            }//tästä saisi helposti suollettua ulos niin tarkat tiedot ja datat kuin haluaa
            //mutta valitsin kuittiin kirjoitettavaksi oleellisimmat kuitin sisältöä ajatellen
            
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ReceiptHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }
   
}
