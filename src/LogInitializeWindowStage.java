
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class LogInitializeWindowStage extends Stage {

    
    public LogInitializeWindowStage(Parent root){
        Scene scene = new Scene(root); 
        scene.getStylesheets().add("LogInitialize_GUI.css"); //css tiedostosta muotoilut käyttöön
        this.setResizable(false);
        this.setScene(scene);
        this.showAndWait();
    }


}
