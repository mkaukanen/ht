
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/** CT60A2411 Olio-ohjelmointi
 *  Harjoitustyö
 *  Tekijät: Kaukanen Miki 0461066
 */

public class ExternalPackageLogHandler {
    private static ExternalPackageLogHandler EPLogger = null;   
    private ArrayList<String> logData;
    
        //SINGLETON
    public static synchronized ExternalPackageLogHandler getExternalPackageLogHandler(){
        if (EPLogger==null){
            EPLogger= new ExternalPackageLogHandler();
        }
        return EPLogger;
    }
    
    public void updateExternalPackageLog(String line){ //lokitiedostoon lisäys
        String fileName = "PackageLog.txt";
        BufferedWriter out;
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), StandardCharsets.ISO_8859_1));
            line=line.replace("\n", ";"); //lukemista varten rivinvaihdot pois, jotta saadaan lokiin helpommin oikein
            out.write(line+"\r\n");
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ExternalPackageLogHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ArrayList<String> getExternalLogData(){
        logData = new ArrayList<String>();
        String fileName = "PackageLog.txt";
        InputStream input;
        try {
            input = new FileInputStream(fileName);
            BufferedReader in;
            in = new BufferedReader (new InputStreamReader(input, StandardCharsets.ISO_8859_1));
            String line;
            while ((line = in.readLine()) != null){ 
                logData.add(line);
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Tiedostoa ei ole vielä olemassa.");
        } catch (IOException ex) {
            Logger.getLogger(ExternalPackageLogHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logData;
    }
    
    
}
